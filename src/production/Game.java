package production;

import java.util.ArrayList;
import java.util.Random;

public class Game {
	
	private Door[] theDoors;
	private final Door winningDoor;
	private Door openedDoor;
	private final int noDoors;
	private final Player thePlayer;
	private final Random theRand;
	
	public Game(int noDoors) {
		
		this.noDoors = noDoors;
		this.theDoors = new Door[this.noDoors];
		
		theRand = new Random();
		
		int winningDoorID = theRand.nextInt(noDoors);
		
		
		for(int i = 0; i < noDoors; i++) {
			theDoors[i] = new Door(i, winningDoorID == i);
		}
		
		winningDoor = theDoors[winningDoorID];
		
		thePlayer = new Player(noDoors);
		
		playSetUp();
		
		
	}
	
	public void playSetUp() {
		
		
		int[] openableDoorIDs = new int[noDoors-1];
		
		ArrayList<Door> openableDoors = new ArrayList<Door>();
		
		
		for(Door i : theDoors) {
			if(i != winningDoor && i.getId() != thePlayer.getPickedDoorID()) {
				openableDoors.add(i);
			}
		}
		
		openedDoor = openableDoors.get(theRand.nextInt(openableDoors.size()));
		
		openedDoor.setOpen();
		
	
	}
	
	public boolean playGameSwap() {
		
		while(!thePlayer.swapDoor(openedDoor.getId())) ;
		
		return thePlayer.getPickedDoorID() == winningDoor.getId();
		
		
	}
	
	public boolean playGameStick() {
		
		return thePlayer.getPickedDoorID() == winningDoor.getId();
		
	}

}
