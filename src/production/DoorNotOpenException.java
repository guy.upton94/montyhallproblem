package production;

public class DoorNotOpenException extends Exception{

	public DoorNotOpenException(String message) {
		super(message);
	}
	
}
