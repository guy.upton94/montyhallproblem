package production;

public class Door {
	
	private final int id;
	private final boolean winner;
	private boolean open;
	
	public Door(int id, boolean winner) {
		this.id = id;
		this.winner = winner;
		open = false;
	}

	public boolean isOpen() {
		return open;
	}

	public void setOpen() {
		this.open = true;
	}

	public int getId() {
		return id;
	}

	public boolean isWinner() throws DoorNotOpenException {
		if(open) {
			return winner;
		}
		throw new DoorNotOpenException("Can't look behind closed door");
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result + (open ? 1231 : 1237);
		result = prime * result + (winner ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Door other = (Door) obj;
		if (id != other.id)
			return false;
		if (open != other.open)
			return false;
		if (winner != other.winner)
			return false;
		return true;
	}

}
