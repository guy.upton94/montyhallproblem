package production;

import java.util.Random;

public class Player {
	
	private int pickedDoorID;
	private boolean changed;
	private final int noDoors;
	private final Random rand;
	
	public Player(int noDoors) {
		this.noDoors = noDoors;
		this.rand = new Random();
		this.pickedDoorID = rand.nextInt(noDoors);
		this.changed = false;
	}

	public int getPickedDoorID() {
		return pickedDoorID;
	}

	public void setPickedDoorID(int pickedDoor) {
		this.pickedDoorID = pickedDoor;
		changed = true;
	}

	public boolean isChanged() {
		return changed;
	}
	
	public boolean swapDoor(int openedDoorID) {
		
		int newDoorID = rand.nextInt(noDoors);
		int oldDoorID = pickedDoorID;
		
		while(newDoorID == openedDoorID || newDoorID == pickedDoorID) {
			newDoorID = rand.nextInt(noDoors);
		}
		
		
		setPickedDoorID(newDoorID);
		
		return isChanged() && oldDoorID != newDoorID;
			
		
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (changed ? 1231 : 1237);
		result = prime * result + noDoors;
		result = prime * result + pickedDoorID;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Player other = (Player) obj;
		if (changed != other.changed)
			return false;
		if (noDoors != other.noDoors)
			return false;
		if (pickedDoorID != other.pickedDoorID)
			return false;
		
		return true;
	}

}
