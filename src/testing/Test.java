package testing;

import production.Game;

public class Test {
	
	public static void main(String [] args){
		
		int winningSwapCounter = 0;
		int winningStickCounter = 0;
		int noGames = 10000;
		int noDoors = 3;
		
		
		for(int i = 0; i < noGames; i++) {
			if(new Game(noDoors).playGameSwap()) {
				winningSwapCounter++;
			}
		}
		for(int i = 0; i < noGames; i++) {
			if(new Game(noDoors).playGameStick()) {
				winningStickCounter++;
			}
		}
		
		String out = "System played with %d doors" ;
		out += "\nSystem played %d swap games\n\tIt had a success rate of: %.8f";
		out += "\nSystem played %d stick games\n\tIt had a success rate of: %.8f";
		System.out.println(String.format(out, 
				noDoors, 
				noGames, (winningSwapCounter/ (double) noGames),
				noGames, (winningStickCounter/ (double) noGames)));
		
		
	}

}
